<?php

declare(strict_types=1);

class PhoneCodes
{
    public function __construct(
        private string $phone = ''
    ) {
    }

    private function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @throws Exception
     */
    public function convert(string $phone): string
    {
        $this->setPhone($phone);
        $this->phoneCorrect();
        return $this->phone;
    }

    /**
     * @throws Exception
     */
    private function phoneCorrect(): void
    {
        $this->clean()
            ->correct8()
            ->correct7()
            ->checkNumberIsCorrect()
            ->addSymbol(' (', 1)
            ->addSymbol(') ', 6)
            ->addSymbol('-', 11)
            ->addSymbol( '-', 14)
            ->addSymbol('+', 0);
    }

    private function clean(): self
    {
        $this->phone = preg_replace('/[^0-9]*/', '', $this->phone);
        return $this;
    }

    private function correct8(): self
    {
        if ($this->phone[0] === '8') {
            $this->phone = '7' . substr($this->phone, 1);
        }
        return $this;
    }

    private function correct7(): self
    {
        if ($this->phone[0] === '9') {
            $this->phone = '7' . $this->phone;
        }
        return $this;
    }

    private function addSymbol(string $symbol,  int $position): self
    {
        $this->phone = (substr($this->phone, 0, $position)) . $symbol . (substr($this->phone, $position));
        return $this;
    }

    private function checkNumberIsCorrect(): self
    {
        if (mb_strlen($this->phone) !== 11) {
            throw new Exception('Number is not correct');
        }
        return $this;
    }
}
